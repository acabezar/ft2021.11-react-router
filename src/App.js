import './App.css';
import {
  BrowserRouter,
  Route,
  Routes,
  Outlet,
  Link,
  NavLink
} from "react-router-dom";
import HomePage from './pages/HomePage/HomePage';
import AboutPage from './pages/AboutPage/AboutPage';
import MoviesPage from './pages/MoviesPage/MoviesPage';
import ContactPage from './pages/ContactPage/ContactPage';
import AboutMePage from './pages/AboutPage/pages/AboutMePage/AboutMePage';
import MoviesDetailPage from './pages/MoviesPage/pages/MoviesDetailPage/MoviesDetailPage';
function App() {
  return (
    <BrowserRouter>

      <header>
        <NavLink to="about" className={({ isActive }) => (isActive ? 'active' : 'inactive')}>About</NavLink>
        <NavLink to="about/me" className={({ isActive }) => (isActive ? 'active' : 'inactive')}>About me</NavLink>
        <NavLink to="movies" className={({ isActive }) => (isActive ? 'active' : 'inactive')}>Movies</NavLink>
      </header>

      <Routes>
        <Route path="/" >

          <Route index element={<HomePage />} />

          {/* <Route path="about" element={<div>asdasdas<Outlet /></div>}> */}
          <Route path="about">
            <Route index element={<AboutPage />} />
            <Route path="me" element={<AboutMePage />} />
          </Route>

          <Route path="movies">
            <Route index element={<MoviesPage />} />
            <Route path=":idMovie" element={<MoviesDetailPage />} />
          </Route>

          <Route path="contact" element={<ContactPage />} />

        </Route>
      </Routes>

      footer

    </BrowserRouter>
  );
}

export default App;
