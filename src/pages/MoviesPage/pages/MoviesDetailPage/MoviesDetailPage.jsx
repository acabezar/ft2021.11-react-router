import { useParams } from "react-router-dom"

export default function MoviesDetailPage() {
    const { idMovie } = useParams();
    return <div>detalle de pelicula {idMovie}</div>
}